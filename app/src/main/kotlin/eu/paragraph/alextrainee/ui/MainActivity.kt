package eu.paragraph.alextrainee.ui

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.fragment.app.FragmentContainerView
import eu.paragraph.alextrainee.R
import eu.paragraph.alextrainee.data.RestApi

class MainActivity : AppCompatActivity() {

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val button = findViewById<Button>(R.id.buttonKek)

        button.setOnClickListener {
            supportFragmentManager.beginTransaction()
                .add(R.id.fmContainer, BlankFragment())
                .commit()
        }
    }

}