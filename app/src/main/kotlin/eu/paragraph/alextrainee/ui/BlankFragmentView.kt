package eu.paragraph.alextrainee.ui

interface BlankFragmentView {
    fun setTextFromData(list: List<Int>)
}