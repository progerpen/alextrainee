package eu.paragraph.alextrainee.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import eu.paragraph.alextrainee.R
import eu.paragraph.alextrainee.presenter.BlankPresenter
import kotlinx.coroutines.launch

class BlankFragment : Fragment(R.layout.fragment_blank), BlankFragmentView {

    // MVP pattern
    // M - Model (то, что загружаем из инета)
    // V - View (фрагмент)
    // P - Presenter (то, где загрузка происходит)

    // DZ: изучать что-нибудь по ХМЛ (ConstraintLayout, LinearLayout и проч) + как связать View (fragment) и xml (findViewById)
    // DZ: на примере того что уже написано попробовать передать какие-то данные (без задержки) во фрагмент
    // DZ: Создать свой фрагмент с изи дизайном: 3 текста друг под друга с отспуами + 2 кнопки под ними в одной строке

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val presenter = BlankPresenter()
        presenter.fragment = this


        val jopa = getView()?.findViewById<Button>(R.id.buttonKek2)
        jopa?.setOnClickListener {
            presenter.getDataFromApi()
        }
    }

    override fun setTextFromData(list: List<Int>) {
        val text = getView()?.findViewById<TextView>(R.id.jopa)
        text?.text = list.toString()
    }
}