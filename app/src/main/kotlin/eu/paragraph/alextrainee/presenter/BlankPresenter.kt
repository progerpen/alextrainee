package eu.paragraph.alextrainee.presenter

import eu.paragraph.alextrainee.data.RestApi
import eu.paragraph.alextrainee.ui.BlankFragment

class BlankPresenter {
    private val restApi = RestApi
    var fragment: BlankFragment? = null

    fun getDataFromApi() {
        fragment?.setTextFromData(restApi.getData())
    }

    fun f1() = listOf("")
    fun f2() = listOf("")
    fun f3() = listOf("")
    fun f4() = listOf("")
}